# Spherical POV Display 3D Printed Parts

## General
Collection of STL models used for the Spherical POV Display.

## IMPORTANT
This design is made for a 6mm rod and 6/15mm bearings.

## Requirements
Designed to be printed on an Anycubic i3 Mega with a build volume of 210mm x 210mm x 205mm.
Any printer that allows these dimensions can be used.
Note models have to rotated and the ring needs to be separated into upper and lower half.
